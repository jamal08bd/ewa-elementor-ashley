var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    jshint        = require('gulp-jshint'),
    notify        = require('gulp-notify'),
    plumber       = require('gulp-plumber'),
    concat        = require('gulp-concat'),
    sourcemaps    = require('gulp-sourcemaps'),
    uglify        = require('gulp-uglify'),
    rename        = require('gulp-rename'),
    autoprefixer  = require('gulp-autoprefixer'),
    cleanCss      = require('gulp-clean-css'),
    htmlValidator = require('gulp-w3c-html-validator'),
    a11y          = require('gulp-accessibility');

gulp.task('sass', function() {
  gulp.src('assets/sass/style.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compact'}).on("error", notify.onError({
    message: "Error: <%= error.message %>",
    title: "Gulp compilation failed."
  })))
  .pipe(autoprefixer({
    browsers: ['last 2 versions']
  }))
  .pipe(gulp.dest('.'))
  .pipe(sourcemaps.write('./'))
  .pipe(cleanCss({
    minify: true,
    collapseWhitespace: true,
    conservativeCollapse: true,
    minifyJS: true,
    minifyCSS: true,
    getKeptComment: function (content, filePath) {
      var m = content.match(/\/\*![\s\S]*?\*\//img);
      return m && m.join('\n') + '\n' || '';
    }
  }))
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('./assets/minified/css'))
  /*.pipe(browserSync.stream())*/;

});

gulp.task('w3c-validate', function()  {
  return gulp.src('*.php', '!functions.php', '!node_modules/**/*')
  .pipe(htmlValidator())
  .pipe(htmlValidator.reporter());

});

gulp.task('a11y', done => {
  return gulp.src(['**/*.php', '!functions.php', '!node_modules/**/*'])
  .pipe(a11y({
      accessibilityLevel: 'WCAG2AA', //WCAG2AA
      verbose: true,
      force: true,
      reportLevels: {
        notice: false,
        warning: false,
        error: true
      },
      ignore: [
        // The html element should have a lang or xml:lang attribute which describes the language of the document.
        'WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2',
        // A title should be provided for the document, using a non-empty title element in the head section.
        'WCAG2AA.Principle2.Guideline2_4.2_4_2.H25.1.NoTitleEl',
        // Anchor element found with a valid href attribute, but no link content has been supplied.
        'WCAG2AA.Principle4.Guideline4_1.4_1_2.H91.A.NoContent',
        // Heading tag found with no content. Text that is not intended as a heading should not be marked up with heading tags.
        'WCAG2AA.Principle1.Guideline1_3.1_3_1.H42.2',
        // This link points to a named anchor "[link target]" within the document, but no anchor exists with that name.
        'WCAG2AA.Principle2.Guideline2_4.2_4_1.G1,G123,G124.NoSuchID'
        ]
      }))
  .on('error', console.log)
  .pipe(a11y.report({reportType: 'txt'}))
  .pipe(rename({
    extname: '.txt'
  }))
  .pipe(gulp.dest('assets/reports/txt'));
});

gulp.task('minifyJS', function() {
  return gulp.src(['assets/js/*.js', '!./js/all.min.js'])
  .pipe(sourcemaps.init())
  .pipe(concat('scripts.min.js'))
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./assets/minified/js'))
});


gulp.task('watch', ["sass", "minifyJS", "a11y"], function() {
  gulp.watch('assets/sass/**/*.scss', [ "sass" ])/*.on('change', browserSync.reload)*/;
  gulp.watch('assets/js/*.js', [ "minifyJS" ])/*.on('change', browserSync.reload)*/;
  //gulp.watch("*.php").on('change', browserSync.reload);

});


gulp.task('default', ['sass', 'minifyJS', 'a11y'],
  function() {
    console.log("Build Success");
  });