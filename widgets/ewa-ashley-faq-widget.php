<?php
/**
 * EWA Elementor Faq Widget.
 *
 * Elementor widget that inserts faq into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Faq_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve faq widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-faq-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve faq widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Faq', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve faq widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the faq widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register faq widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Faq Title
		$this->add_control(
		    'ewa_faq_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Faq Title','ewa-elementor-ashley'),
			]
		);
		
		// Faq  Sub Title
		$this->add_control(
		    'ewa_faq_sub_title',
			[
			    'label' => esc_html__('Sub Title','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Faq Sub Title','ewa-elementor-ashley'),
			]
		);
		
		// Faq repeater
		$repeater = new \Elementor\Repeater();

		// Repeater for Accordion Title
		$repeater->add_control(
			'ewa_faq_accordion_title',
			[
				'label' => esc_html__( 'Accordion Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Add New Accordion' , 'ewa-elementor-ashley' ),
			]
		);
		
		// Repeater for Accordion Inner
		$repeater->add_control(
			'ewa_faq_accordion_inner',
			[
				'label' => esc_html__( 'Accordion Inner', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'label_block' => true,
				'default' => esc_html__( 'Add New Accordion Inner' , 'ewa-elementor-ashley' ),
			]
		);
		
		// Accordion List
		$this->add_control(
			'ewa_faq_accordion_list',
			[
				'label' => esc_html__( 'Accordion List', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_faq_accordion_title }}}',
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Faq Content Options
		$this->add_control(
			'ewa_faq_content_options',
			[
				'label' => esc_html__( 'Content', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Faq Title Color
		$this->add_control(
			'ewa_faq_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .faq__content h5' => 'color: {{VALUE}}',
				],
			]
		);

		// Faq Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_faq_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .faq__content h5',
			]
		);
		
		// Faq Sub Title Color
		$this->add_control(
			'ewa_faq_sub_title_color',
			[
				'label' => esc_html__( 'Subtitle Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .faq__content p' => 'color: {{VALUE}}',
				],
			]
		);

		// Faq Sub Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_faq_sub_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .faq__content p',
			]
		);
		
		// Faq Accordion Options
		$this->add_control(
			'ewa_faq_accordion_options',
			[
				'label' => esc_html__( 'Accordion Content', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Faq Accordion Background Color
		$this->add_control(
			'ewa_faq_accordion_back_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .faq ul.faq-accordion li, .faq ul.faq-accordion li a.accordion-toggle.current' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Faq Accordion Title Color
		$this->add_control(
			'ewa_faq_accordion_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .accordion-toggle' => 'color: {{VALUE}}',
				],
			]
		);

		// Faq Accordion Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_faq_accordion_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .accordion-toggle',
			]
		);
		
		// Faq Accordion Inner Text Color
		$this->add_control(
			'ewa_faq_accordion_inner_text_color',
			[
				'label' => esc_html__( 'Description Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .accordion-inner p' => 'color: {{VALUE}}',
				],
			]
		);

		// Faq Accordion Inner Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_faq_accordion_inner_text_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .faq ul.faq-accordion .accordion-inner',
			]
		);
		
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render faq widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		
		$faq_title = $settings['ewa_faq_title'];
		$faq_sub_title = $settings['ewa_faq_sub_title'];
	
		
		
       ?>
		<!-- Faq Area Start Here -->		
		    <div class="faq">
			    <div class="faq__content">
				    <h5><?php echo $faq_title; ?></h5>
					<p>(<?php echo $faq_sub_title?>)</p>
				</div> <!-- end of .faq__content -->
				<div class="faq__list">
					<ul class="faq-accordion">
						<?php 

						$count = 0;
						// we will define which one needs to be opened initially, in this case we will define 1st accordion as current faq
						$open_faq_class = '';
						$open_faq_inner = '';

						foreach( $settings['ewa_faq_accordion_list'] as $item ) { 
							$faq_accordion_title = $item['ewa_faq_accordion_title'];
							$faq_accordion_inner = $item['ewa_faq_accordion_inner'];


							// define 1st element as initial open element (DO NOT REMOVE THIS, THIS NEED SOME FIX)
							if ($count == 0) {
								$open_faq_class = ' current';
								$open_faq_inner = ' show';
							} else{
								$open_faq_class = '';
								$open_faq_inner = '';
							}

						?>								
							<li>									
								<a class="accordion-toggle<?php echo esc_attr($open_faq_class); ?>" href="javascript:void(0);">
									<?php echo $faq_accordion_title;?>
								</a>
								<div class="accordion-inner<?php echo esc_attr($open_faq_inner); ?>">
									<?php echo $faq_accordion_inner;?>
								</div>
							</li>
						<?php
						$count++; 
						} 
						?>
					</ul><!-- .end of .ashley-accordion -->	
				</div> <!-- end of .faq__accordion -->
			</div>
		<!-- Faq Area End Here -->
       <?php
	}
}