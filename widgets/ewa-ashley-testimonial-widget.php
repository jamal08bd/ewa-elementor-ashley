<?php
/**
 * EWA Elementor Testimonial Widget.
 *
 * Elementor widget that inserts testimonial into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Testimonial_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve testimonial widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-testimonial-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve testimonial widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Testimonial', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve testimonial widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the testimonial widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register testimonial widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Testimonial Heading
		$this->add_control(
		    'ewa_testimonial_heading',
			[
			    'label' => esc_html__('Testimonial Heading','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Heading','ewa-elementor-ashley'),
			]
		);
		
		// Testimonial Repeater
		$repeater = new \Elementor\Repeater();
		
		// Testimonial Description
		$repeater->add_control(
			'ewa_testimonial_des',
			[
				'label' => esc_html__( 'Description', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__( 'Enter Description' , 'ewa-elementor-ashley' ),
			]
		);
		
		// Testimonial Name
		$repeater->add_control(
			'ewa_testimonial_name',
			[
				'label' => esc_html__( 'Author Name', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Name' , 'ewa-elementor-ashley' ),
			]
		);

        // Testimonial Designation
		$repeater->add_control(
			'ewa_testimonial_designation',
			[
				'label' => esc_html__( 'Designation', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Enter Designation' , 'ewa-elementor-ashley' ),
			]
		);
		
		//Testimonial Company Name
		$repeater->add_control(
		    'ewa_testimonial_company_name',
			[
			    'label' => esc_html__('Company Name','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Company Name','ewa-elementor-ashley'),
			]
		);
		
		// Testimonial List
		$this->add_control(
			'ewa_testimonial_list',
			[
				'label' => esc_html__( 'Testimonials List', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_testimonial_des }}}',
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Testimonial Heading Options
		$this->add_control(
			'ewa_testimonial_heading_options',
			[
				'label' => esc_html__( 'Testimonial Heading', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Testimonial Heading Color
		$this->add_control(
			'ewa_testimonial_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .testimonial h2' => 'color: {{VALUE}}',
				],
			]
		);

		// Testimonial Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_testimonial_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonial h2',
			]
		);
		
		// Testimonial Background Options
		$this->add_control(
			'ewa_testimonial_background_options',
			[
				'label' => esc_html__( 'Testimonial Background', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Testimonial Area Background Color
		$this->add_control(
			'ewa_testimonial_area_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => 'rgba(252, 215, 217, 0.6);',
				'selectors' => [
					'{{WRAPPER}} .testimonial__area' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Testimonial Description Options
		$this->add_control(
			'ewa_testimonial_description_options',
			[
				'label' => esc_html__( 'Testimonial Description ', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Testimonial Description Color
		$this->add_control(
			'ewa_testimonial_description_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .testimonial__items p' => 'color: {{VALUE}}',
				],
			]
		);

		// Testimonial Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_testimonial_desc_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonial__items p',
			]
		);
		
		// Testimonial Author Options
		$this->add_control(
			'ewa_testimonial_author_options',
			[
				'label' => esc_html__( 'Testimonial Author ', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Testimonial Name Color
		$this->add_control(
			'ewa_testimonial_name_color',
			[
				'label' => esc_html__( 'Name Color', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .testimonial__info h4' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Testimonial Name Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_testimonial_name_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonial__info h4',
			]
		);
		
		// Testimonial Designation Color
		$this->add_control(
			'ewa_testimonial_designation_color',
			[
				'label' => esc_html__( 'Designation Color', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .testimonial__info span' => 'color: {{VALUE}}',
				],
			]
		);

		// Testimonial Designation Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_testimonial_designation_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonial__info span',
			]
		);

		// Testimonial Arrow Options
		$this->add_control(
			'ewa_testimonial_arrow_options',
			[
				'label' => esc_html__( 'Testimonial Arrows ', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Testimonial Arrow Color
		$this->add_control(
			'ewa_testimonial_arrow_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .testimonial__area .slick-prev::before, .testimonial__area .slick-next::before' => 'color: {{VALUE}}',
				],
			]
		);

		// Testimonial Arrow Background Color
		$this->add_control(
			'ewa_testimonial_arrow_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .testimonial__area .slick-prev, .testimonial__area .slick-next' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Testimonial Dots Options
		$this->add_control(
			'ewa_testimonial_dots_options',
			[
				'label' => esc_html__( 'Testimonial Dots ', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Testimonial Dots Color
		$this->add_control(
			'ewa_testimonial_dots_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .slick-dots li button::before' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Testimonial Dots Active Color
		$this->add_control(
			'ewa_testimonial_dots_active_color',
			[
				'label' => esc_html__( 'Active Color', 'ewa-elementor-retouch' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .slick-dots li.slick-active button::before' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render testimonial widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$testimonial_heading = $settings['ewa_testimonial_heading'];
		
		
       ?>
		<!-- Testimonial Area Start Here -->		
		    <div class="grid">
			    <div class="col-sm-12">
				    <div class="testimonial">
						<h2><?php echo $testimonial_heading; ?></h2>
						<div class="testimonial__area">
							<?php 
							foreach (  $settings['ewa_testimonial_list'] as $item ) { 
								$testimonial_des = $item['ewa_testimonial_des'];
								$testimonial_name = $item['ewa_testimonial_name'];
								$testimonial_designation = $item['ewa_testimonial_designation'];
								$testimonial_company_name = $item['ewa_testimonial_company_name'];
							?>
								<div class="testimonial__items">
									<p><?php echo $testimonial_des; ?></p>
									<div class="testimonial__info">
										<h4><?php echo $testimonial_name; ?></h4>
										<span><?php echo $testimonial_designation; ?></span><span><?php echo $testimonial_company_name; ?></span>
									</div> <!-- testimonial-area__title end here -->
								</div> <!-- testimonial-area__item -->
							<?php } ?>
						</div> <!-- end of .testimonial__area -->
					</div> <!-- end of .testimonial -->
				</div>
		    </div> <!-- end of .grid -->
		<!-- Testimonial Area End Here -->
       <?php
	}
}