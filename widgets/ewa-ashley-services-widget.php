<?php
/**
 * EWA Elementor Services Widget.
 *
 * Elementor widget that inserts services into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Services_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve services widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-services-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve services widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Services', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve services widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the services widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register services widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Services Repeater
		$repeater = new \Elementor\Repeater();
		
		// Services Image
		$repeater->add_control(
		    'ewa_services_image',
			[
			    'label' => esc_html__('Choose Services Image','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Services Pre Title
		$repeater->add_control(
		    'ewa_services_pre_title',
			[
			    'label' => esc_html__('Pre Title','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Services Pre Title','ewa-elementor-ashley'),
			]
		);
		
		// Services Title
		$repeater->add_control(
		    'ewa_services_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Services Title','ewa-elementor-ashley'),
			]
		);
		
		// Services Description
		$repeater->add_control(
		    'ewa_services_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Services Description','ewa-elementor-ashley'),
			]
		);
		
		// Services Button1 Text
        $repeater->add_control(
        	'ewa_services_button1_text',
			[
				'label'         => esc_html__('Services Button Text', 'ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button1 Text','ewa-elementor-ashley'),
			]
        );
		
		//Services Button1 Link
		$repeater->add_control(
		    'ewa_services_button1_link',
			[
			    'label'         => esc_html__('Services Button Link','ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		// Services List
		$this->add_control(
			'ewa_services_list',
			[
				'label' => esc_html__( 'Services List', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_services_title }}}',
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Services Pre Title Options
		$this->add_control(
			'ewa_services_pre_title_options',
			[
				'label' => esc_html__( 'Services Pre Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Services Pre Title Bar Color
		$this->add_control(
			'ewa_services_pre_title_bar_color',
			[
				'label' => esc_html__( 'Bar Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .services__title h4::after' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Services Pre Title Color
		$this->add_control(
			'ewa_services_pre_title_color',
			[
				'label' => esc_html__( 'Pre Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .services__content h4' => 'color: {{VALUE}}',
				],
			]
		);

		// Services Pre Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_services_pre_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .services__content h4',
			]
		);
		
		// Services Title Options
		$this->add_control(
			'ewa_services_title_options',
			[
				'label' => esc_html__( 'Services Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Services Title Color
		$this->add_control(
			'ewa_services_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .services__content h2 ' => 'color: {{VALUE}}',
				],
			]
		);

		// Services Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_services_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .services__content h2',
			]
		);
		
		// Services Description Options
		$this->add_control(
			'ewa_services_description_options',
			[
				'label' => esc_html__( 'Services Description ', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Services Description Color
		$this->add_control(
			'ewa_services_description_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .services__content p' => 'color: {{VALUE}}',
				],
			]
		);

		// Services Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_services_desc_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .services__content p',
			]
		);
		
		// Services Button Options
		$this->add_control(
			'ewa_services_btn_options',
			[
				'label' => esc_html__( 'Services Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Services Button Color
		$this->add_control(
			'ewa_services_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .services__content a' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Services Button Background Color
		$this->add_control(
			'ewa_services_btn_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .services__content a' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Services Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_services_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .services__content a',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);

		// Services Button Hover Options
		$this->add_control(
			'ewa_services_btn_options',
			[
				'label' => esc_html__( 'Services Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Services Button Hover Background Color
		$this->add_control(
			'ewa_services_btn_hover_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .btn-primary:hover' => 'background-color: {{VALUE}} !important',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render services widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		//$services_list = $settings['ewa_services_list'];
		
       ?>
		<!-- Services Area Start Here -->		
		    <div class="services">
				<?php 
				foreach (   $settings['ewa_services_list'] as $key => $item ) {
					
					$services_image = $item['ewa_services_image']['url'];
					$services_pre_title = $item['ewa_services_pre_title'];
					$services_title = $item['ewa_services_title'];
					$services_description = $item['ewa_services_des'];
					$services_btn_link = $item['ewa_services_button1_link']['url'];
					$services_btn_text = $item['ewa_services_button1_text'];
					
					$is_odd_even = (($key + 1) % 2) ? 'odd': 'even';

					$box_1_class = '';
					$box_2_class = '';
					$box_main_class = '';

					if ($is_odd_even === 'odd') {
						$box_1_class = 'offset-md ';
						$box_2_class = '';
						$box_main_class = 'odd-service';
					} else {
						$box_1_class = 'order-sm-2 ';
						$box_2_class = 'offset-md ';
						$box_main_class = 'even-service';
					}
				
				?>
				    <div class="grid <?= $box_main_class; ?>">					
						<div class="<?= $box_1_class; ?>col-md-6 col-sm-6 col-xs-12">
							<div class="services__image">
								<img src="<?php echo $services_image; ?>" alt="" />
							</div>  <!-- end of .services__image -->
						</div>  <!-- end of .col -->
						<div class="<?= $box_2_class; ?>col-md-6 col-sm-6 col-xs-12 align-self-center">
							<div class="services__content">
								<div class="services__title">
									<h4><?php echo $services_pre_title; ?></h4>
								</div>
								<h2><?php echo $services_title; ?></h2>
								<p><?php echo $services_description; ?></p>
								<a href="<?php echo esc_url($services_btn_link);?>"title="<?php echo $services_btn_text;?>"class="btn-primary btn-primary-icon"><?php echo $services_btn_text; ?></a>
							</div>
						</div>  <!-- end of .col -->					
				    </div> <!-- end of .grid -->
				<?php } ?>
			</div>
		<!-- Services Area End Here -->
       <?php
	}
}