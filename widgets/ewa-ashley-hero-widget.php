<?php
/**
 * EWA Elementor Hero Widget.
 *
 * Elementor widget that inserts hero into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Hero_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve hero widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-hero-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve hero widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Hero', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve hero widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the hero widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register hero widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Hero Image
		$this->add_control(
		    'ewa_hero_image',
			[
			    'label' => esc_html__('Choose Hero Image','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Hero Title
		$this->add_control(
		    'ewa_hero_sign',
			[
			    'label' => esc_html__('Signature','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Hero Signature','ewa-elementor-ashley'),
			]
		);
		
		// Hero Title
		$this->add_control(
		    'ewa_hero_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Hero Title','ewa-elementor-ashley'),
			]
		);
		
		// Hero Button1 Text
        $this->add_control(
        	'ewa_hero_button1_text',
			[
				'label'         => esc_html__('Hero Button Text', 'ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button1 Text','ewa-elementor-ashley'),
			]
        );
		
		//Hero Button1 Link
		$this->add_control(
		    'ewa_hero_button1_link',
			[
			    'label'         => esc_html__('Hero Button Link','ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Hero Title Options
		$this->add_control(
			'ewa_hero_title_options',
			[
				'label' => esc_html__( 'Hero Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Title Color
		$this->add_control(
			'ewa_hero_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .hero__content h1' => 'color: {{VALUE}}',
				],
			]
		);

		// Hero Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero__content h1',
			]
		);
		
		// Hero Sign Options
		$this->add_control(
			'ewa_hero_sign_options',
			[
				'label' => esc_html__( 'Hero Signature', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Sign Color
		$this->add_control(
			'ewa_hero_sign_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .hero__sign span' => 'color: {{VALUE}}',
				],
			]
		);

		// Hero Sign Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_sign_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero__sign',
			]
		);
		
		// Hero Button Options
		$this->add_control(
			'ewa_hero_btn_options',
			[
				'label' => esc_html__( 'Hero Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Button Color
		$this->add_control(
			'ewa_hero_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .hero__btn' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Hero Button Background Color
		$this->add_control(
			'ewa_hero_btn_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .hero__btn' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Hero Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_button_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero__content a',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);

		// Hero Button Hover Options
		$this->add_control(
			'ewa_hero_btn_hover_options',
			[
				'label' => esc_html__( 'Hero Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Button Hover Background Color
		$this->add_control(
			'ewa_hero_btn_hover_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .btn-primary.btn-primary-icon:hover' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render hero widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$hero_image = $settings['ewa_hero_image']['url'];
		$hero_sign = $settings['ewa_hero_sign'];
		$hero_title = $settings['ewa_hero_title'];
		$hero_btn_link = $settings['ewa_hero_button1_link']['url'];
		$hero_btn_text = $settings['ewa_hero_button1_text'];
		
		
       ?>
		<!-- Hero Area Start Here -->		
		    <div class="hero">
				<div class="hero__content">
				    <h1><?php echo $hero_title; ?></h1>
				    <a href="<?php echo esc_url($hero_btn_link);?>" title="<?php echo $hero_btn_text; ?>" class="btn-primary btn-primary-icon"><?php echo $hero_btn_text; ?></a>
				</div> <!-- end of .hero__content -->
				<div class="hero__outter">
					<div class="hero__image" style="background-image: url('<?php echo esc_url($hero_image); ?>');"></div> 
					<div class="hero__sign"><?php echo $hero_sign; ?></div>
				</div> <!-- end of .hero__outter -->
			</div> <!-- end of .hero -->
		<!-- Hero Area End Here -->
       <?php
	}
}