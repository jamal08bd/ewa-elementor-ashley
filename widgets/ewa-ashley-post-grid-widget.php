<?php
/**
 * EWA Elementor Post Grid Widget.
 *
 * Elementor widget that inserts post grid into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Post_Grid_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve post grid widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-post-grid-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve post grid widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Post Grid', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve post grid widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the post grid widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register post grid widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Post Grid Title
		$this->add_control(
			'ewa_post_grid_title',
			[
				'label' => esc_html__( 'Grid Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter Grid title', 'ewa-elementor-ashley' ),
			]
		);
		
		// Post Grid Tab Title One
		$this->add_control(
			'ewa_post_grid_tab_title_one',
			[
				'label' => esc_html__( 'Tab Title One', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter Tab title', 'ewa-elementor-ashley' ),
			]
		);

		// Post Grid Tab One Total Posts
		$this->add_control(
			'ewa_post_grid_tab_two_total_posts',
			[
				'label' => esc_html__( 'Tab One Total Posts', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 6,
				'placeholder' => esc_html__( 'Enter Tab One Total Posts', 'ewa-elementor-ashley' ),
			]
		);
		
		// Post Grid Tab Title Two
		$this->add_control(
			'ewa_post_grid_tab_title_two',
			[
				'label' => esc_html__( 'Tab Title Two', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter Tab title two', 'ewa-elementor-ashley' ),
			]
		);

		// Post Grid Tab Two Total Posts
		$this->add_control(
			'ewa_post_grid_tab_one_total_posts',
			[
				'label' => esc_html__( 'Tab Two Total Posts', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 6,
				'placeholder' => esc_html__( 'Enter Tab Two Total Posts', 'ewa-elementor-ashley' ),
			]
		);
		
		// Post Grid Button1 Text
        $this->add_control(
        	'ewa_post_grid_button1_text',
			[
				'label'         => esc_html__('Post Grid Button Text', 'ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button1 Text','ewa-elementor-ashley'),
			]
        );
		
		//Post Grid Button1 Link
		$this->add_control(
		    'ewa_post_grid_button1_link',
			[
			    'label'         => esc_html__('Post Grid Button1 Link','ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		
		// Source of the posts
		$this->start_controls_section(
			'source_section',
			[
				'label' => esc_html__( 'Posts Source', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        // Categories slugs for the blog posts	
		$this->add_control(
			'ewa_posts_from_categories_by_slugs',
			[
				'label' => esc_html__( 'Post from Categories (Enter Category slugs separated by comma)', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'category-slug1, category-slug2', 'ewa-elementor-ashley' ),
			]
		);
			
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Post Grid Title Options
		$this->add_control(
			'ewa_post_grid_title_options',
			[
				'label' => esc_html__( 'Post Grid Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Grid Title Color
		$this->add_control(
			'ewa_post_grid_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .post-grid__heading-title' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Grid Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_title_color_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .post-grid__heading-title',
			]
		);
		
		// Post Grid Bar Background Color
		$this->add_control(
			'ewa_post_grid_bar_background_color',
			[
				'label' => esc_html__( 'Separator Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .post-grid__heading-title::before' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Post Grid Content Options
		$this->add_control(
			'ewa_post_grid_content_options',
			[
				'label' => esc_html__( 'Post Grid Content', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// Post Grid Content Background Color
		$this->add_control(
			'ewa_post_grid_content_background_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => 'rgba(63, 0, 0, 0.5)',
				'selectors' => [
					'{{WRAPPER}} .post-grid__content' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// Post Grid Content Title Color
		$this->add_control(
			'ewa_post_grid_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .post-grid__content span a' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Grid Content Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .post-grid__content span a',
			]
		);

		// Post Grid Content Meta Color
		$this->add_control(
			'ewa_post_grid_meta_color',
			[
				'label' => esc_html__( 'Meta Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .post-grid__author, .post-grid__date' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Grid Content Meta Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_meta_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .post-grid__author, .post-grid__date',
			]
		);
		
		// Post Grid Tabs Options
		$this->add_control(
			'ewa_post_grid_tabs_options',
			[
				'label' => esc_html__( 'Post Grid Tabs', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Post Grid Tabs Box Border
		$this->add_control(
			'ewa_post_grid_tabs_border',
			[
				'label' => esc_html__( 'Box Border Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .post-grid__tabs' => 'border: 1px solid {{VALUE}}',
				],
			]
		);

		// Post Grid Tabs Title Border
		$this->add_control(
			'ewa_post_grid_title_border',
			[
				'label' => esc_html__( 'Title Border Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} ul.ashley-news-tabs li' => 'border: 1px solid {{VALUE}}',
				],
			]
		);

		// Post Grid Tabs Title Color
		$this->add_control(
			'ewa_post_grid_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} ul.ashley-news-tabs li' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Grid Tabs Title Background
		$this->add_control(
			'ewa_post_grid_title_background',
			[
				'label' => esc_html__( 'Title Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} ul.ashley-news-tabs li.current' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Post Grid Tabs Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ul.ashley-news-tabs li',
			]
		);
		
		// Post Grid Article Options
		$this->add_control(
			'ewa_post_grid_article_options',
			[
				'label' => esc_html__( 'Post Grid Article', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Post Grid Article Title Color
		$this->add_control(
			'ewa_post_grid_article_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .template-list-thumbnail__title' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Grid Article Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_article_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .template-list-thumbnail__title',
			]
		);

		// Post Grid Article Meta Color
		$this->add_control(
			'ewa_post_grid_article_meta_color',
			[
				'label' => esc_html__( 'Meta Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#756767',
				'selectors' => [
					'{{WRAPPER}} .template-list-thumbnail a.meta-cat, .template-list-thumbnail__meta' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Grid Article Meta Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_article_meta_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .template-list-thumbnail a.meta-cat, .template-list-thumbnail__meta',
			]
		);
		
		// Post Grid Button Options
		$this->add_control(
			'ewa_post_grid_button_options',
			[
				'label' => esc_html__( 'Post Grid More Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Post Grid Button Background
		$this->add_control(
			'ewa_post_grid_btn_background',
			[
				'label' => esc_html__( 'Button Background', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .post-grid__btn' => 'background: {{VALUE}}',
				],
			]
		);
		
		// Post Grid Button Color
		$this->add_control(
			'ewa_post_grid_btn_color',
			[
				'label' => esc_html__( 'Button Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .post-grid__btn' => 'color: {{VALUE}}',
				],
			]
		);

		// Post Grid Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_post_grid_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .post-grid__btn',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);

		// Post Grid Title Hover Color
		$this->add_control(
			'ewa_post_grid_title_hover_color',
			[
				'label' => esc_html__( 'Post Grid Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .post-grid__content span a:hover' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Grid Meta Hover Color
		$this->add_control(
			'ewa_post_grid_meta_hover_color',
			[
				'label' => esc_html__( 'Post Grid Meta', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .post-grid__author:hover, .post-grid__date:hover' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Article Title Hover Color
		$this->add_control(
			'ewa_post_post_article_title_hover_color',
			[
				'label' => esc_html__( 'Post Article Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#921F47',
				'selectors' => [
					'{{WRAPPER}} .template-list-thumbnail__title:hover' => 'color: {{VALUE}} !important',
				],
			]
		);

		// Post Article Meta Hover Color
		$this->add_control(
			'ewa_post_grid_article_hover_color',
			[
				'label' => esc_html__( 'Post Article Meta Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#921F47',
				'selectors' => [
					'{{WRAPPER}} .template-list-thumbnail a.meta-cat:hover, .template-list-thumbnail__meta time.meta-date:hover' => 'color: {{VALUE}}  !important',
				],
			]
		);

		// Post Grid Button Hover Background
		$this->add_control(
			'ewa_post_grid_btn_hover_background',
			[
				'label' => esc_html__( 'Button Background', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .post-grid__btn:hover' => 'background: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render post grid widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		// getting post grid title
		$post_grid_title = $settings['ewa_post_grid_title'];
		
		// getting post grid tab title's
		$post_grid_tab_title_one = $settings['ewa_post_grid_tab_title_one'];
		$post_grid_tab_title_two = $settings['ewa_post_grid_tab_title_two'];

		// getting post grid tab total's
		$post_grid_tab_one_totals = $settings['ewa_post_grid_tab_one_total_posts'];
		$post_grid_tab_two_totals = $settings['ewa_post_grid_tab_two_total_posts'];

		// Post from categories getting category Slug's
		$cat_slugs = $settings['ewa_posts_from_categories_by_slugs'];
		
		// get post grid button link and text
		$post_grid_btn_link = $settings['ewa_post_grid_button1_link']['url'];
		$post_grid_btn_text = $settings['ewa_post_grid_button1_text'];

		// Get IDs of sticky posts
        $sticky = get_option( 'sticky_posts' );

        $args = array(
          'post_type' => 'post',
          'post_status' => 'publish',
		  'posts_per_page' => 1,
		  'post__in' => $sticky, 
		  'ignore_sticky_posts' => 1, 
          'category_name' => $cat_slugs,
		  'orderby' => 'date', 
        );
        $query_featured_post = new WP_Query($args);
		
		// for unique tab name
		$data_tab_id = 'ashley';
		// args and query for the trendy posts 
	    $args_trendy_post = [
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $post_grid_tab_one_totals,
			'ignore_sticky_posts' => 1,
			'category_name' =>  $cat_slugs,
			'meta_key' => 'total_views',
			'orderby' => [ 'meta_value_num' => 'DESC', 'title' => 'ASC' ],
			'date_query'    => ['column'  => 'post_date','after'   => '- 180 days']
	    ];

	    $query_trendy_post = new WP_Query($args_trendy_post);


	    // args and query for the recent posts  
	    $args_recent_post = [
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $post_grid_tab_two_totals,
			'ignore_sticky_posts' => 1,
			'category_name' =>  $cat_slugs,
			'orderby' => 'date',
	    ];

	    $query_recent_post = new WP_Query($args_recent_post);
		
       ?>
        <div class="post-grid">
			<!-- post grid Area Start Here -->		
			<div class="post-grid__heading">
				<span class="post-grid__heading-title"><?php echo $post_grid_title;?></span>
			</div> <!-- section-heading end here -->	
			<div class="grid">
			    <!-- Featured posts output start -->
	 			<div class="col-lg-8 col-sm-6">
				    <div class="post-grid__classic">
						<?php 
						if ($query_featured_post->have_posts()) :

						  while ($query_featured_post->have_posts()) : $query_featured_post->the_post(); 

							// get the featured image url
							$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );

							$cat_name = ashley_article_primary_category_name( get_the_ID() );
							$cat_link = ashley_article_primary_category_link( get_the_ID() );

						?>				
							<div class="post-grid__items">
								<a href="<?php esc_url(the_permalink()); ?>"><div class="post-grid__image" style="background-image: url('<?php echo esc_url($image_url); ?>');"></div></a>
								<div class="post-grid__content">
									<span class="post-grid__title"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></span>
								    <div class="post-grid__details">
									    <a class="post-grid__author" href="<?php echo esc_url($cat_link); ?>" rel="category"><i class="far fa-folder-open"></i> <?php echo $cat_name; ?></a>
								        <div class="post-grid__date"><i class="far fa-calendar"></i> <?php echo get_the_date(); ?></div>
									</div>
								</div> <!-- end of .post-grid__content -->
							</div> <!-- end of .post-grid__items -->
						<?php
		  		        endwhile;
		                endif; wp_reset_query(); 
						?>
					</div> <!-- end of .post-grid__classic -->
				</div> <!-- Featured posts end -->
				<!-- Trendy and Recent posts output start -->
				<div class="col-lg-4 col-sm-6">
				    <div class="post-grid__tabs">
						<!-- Tabs for both the New and Popular posts -->
						<ul class="ashley-news-tabs tab-item-half">
						  <li class="ashley-tab-link current" data-tab="<?= $data_tab_id; ?>-tab-1"><?php echo $post_grid_tab_title_one;?></li>
						  <li class="ashley-tab-link" data-tab="<?= $data_tab_id; ?>-tab-2"><?php echo $post_grid_tab_title_two;?></li>
						</ul><!-- end of .ashley-news-tabs -->
							
						<!-- Display Content for Trendy posts -->
						<?php if ($query_trendy_post->have_posts()) : ?>
						    <div id="<?= $data_tab_id; ?>-tab-1" class="ashley-tab-content current">
								<?php
								while ($query_trendy_post->have_posts()) : $query_trendy_post->the_post();
								// get the featured image url
								$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
								  
						        ?>    
									<div class="template-list-thumbnail">
										<a href="<?php the_permalink(); ?>" class="template-list-thumbnail__image-url">
											<div class="template-list-thumbnail__image"  style="background-image: url('<?php echo esc_url($image_url); ?>');"></div>
										</a>								
										<div class="template-list-thumbnail__content">
											<a href="<?php the_permalink(); ?>" class="template-list-thumbnail__title-url"><h5 class="template-list-thumbnail__title"><?php the_title(); ?></h5></a>
											<div class="template-list-thumbnail__meta">
												<?php echo ashley_article_primary_category(get_the_ID()); ?>
												<span class="meta-devider">|</span>
												<time class="meta-date" datetime="<?php echo get_the_date(); ?>" ><?php echo get_the_date(); ?></time>
											</div><!-- end of .template-list-thumbnail__meta -->
										</div><!-- end of .template-list-thumbnail__content -->									
									</div><!-- end of .template-list-thumbnail -->			
								<?php endwhile;?>
						    </div><!-- end of .ashley-tab-content -->
						<?php endif; wp_reset_query(); ?>

						<!-- Display Content for Recent posts -->
						<?php if ($query_recent_post->have_posts()) : ?>
						    <div id="<?= $data_tab_id; ?>-tab-2" class="ashley-tab-content">
								<?php
								while ($query_recent_post->have_posts()) : $query_recent_post->the_post(); 					
								// get the featured image url
								$image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
									  
								?>    
									<div class="template-list-thumbnail">
										<a href="<?php the_permalink(); ?>" class="template-list-thumbnail__image-url">
											<div class="template-list-thumbnail__image"  style="background-image: url('<?php echo esc_url($image_url); ?>');"></div>
										</a>								
										<div class="template-list-thumbnail__content">
											<a href="<?php the_permalink(); ?>" class="template-list-thumbnail__title-url"><h5 class="template-list-thumbnail__title"><?php the_title(); ?></h5></a>
											<div class="template-list-thumbnail__meta">
												<?php echo ashley_article_primary_category(get_the_ID()); ?>
												<span class="meta-devider">|</span>
												<time class="meta-date" datetime="<?php echo get_the_date(); ?>" ><?php echo get_the_date(); ?></time>
											</div><!-- end of .template-list-thumbnail__meta -->
										</div><!-- end of .template-list-thumbnail__content -->									
									</div><!-- end of .template-list-thumbnail -->					
								<?php endwhile;?>
						    </div><!-- end of .ashley-tab-content -->
						<?php endif; wp_reset_query(); ?>
					</div> <!-- end of .post-grid__tabs -->
			    </div> <!-- end of .col -->
			</div><!-- end of .grid -->

			<a href="<?php echo esc_url($post_grid_btn_link);?>" title="<?php echo $post_grid_btn_text;?>"  class="post-grid__btn"><?php echo $post_grid_btn_text;?></a><!-- end of .post-grid__bottom -->

		</div><!-- end of .post-grid -->
		<!-- post grid Area End Here -->
       <?php
	}
}