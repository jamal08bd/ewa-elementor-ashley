<?php
/**
 * EWA Elementor Hero Two Widget.
 *
 * Elementor widget that inserts hero two into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Hero_Two_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve hero two widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-hero-two-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve hero two widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Hero Two', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve hero two widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the hero two widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register hero two widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Hero Two Image
		$this->add_control(
		    'ewa_hero_two_image',
			[
			    'label' => esc_html__('Choose Hero Two Image','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// Hero Two Title
		$this->add_control(
		    'ewa_hero_two_title',
			[
			    'label' => esc_html__('Title','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Hero Two Title','ewa-elementor-ashley'),
			]
		);
		
		// Hero Two Button1 Text
        $this->add_control(
        	'ewa_hero_two_button1_text',
			[
				'label'         => esc_html__('Hero Two Button Text', 'ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button Text','ewa-elementor-ashley'),
			]
        );
		
		//Hero Two Button1 Link
		$this->add_control(
		    'ewa_hero_two_button1_link',
			[
			    'label'         => esc_html__('Hero Two Button Link','ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Hero Two Title Options
		$this->add_control(
			'ewa_hero_two_title_options',
			[
				'label' => esc_html__( 'Hero Two Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Two Title Color
		$this->add_control(
			'ewa_hero_two_title_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .hero-two__content h2 ' => 'color: {{VALUE}}',
				],
			]
		);

		// Hero Two Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_two_desc_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-two__content h2',
			]
		);
		
		// Hero Two Button Options
		$this->add_control(
			'ewa_hero_btn_options',
			[
				'label' => esc_html__( 'Hero Two Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Two Button Color
		$this->add_control(
			'ewa_hero_two_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .hero-two__btn' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Hero Two Button Background Color
		$this->add_control(
			'ewa_hero_two_btn_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .hero-two__btn' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Hero Two Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_hero_two_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .hero-two__content a',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);

		// Hero Two Button Hover Options
		$this->add_control(
			'ewa_hero_btn_hover_options',
			[
				'label' => esc_html__( 'Hero Two Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Hero Two Button Hover Background Color
		$this->add_control(
			'ewa_hero_two_btn_hover_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .btn-primary.btn-primary-icon:hover' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render hero two widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$hero_two_image = $settings['ewa_hero_two_image']['url'];
		$hero_two_title = $settings['ewa_hero_two_title'];
		$hero_two_btn_link = $settings['ewa_hero_two_button1_link']['url'];
		$hero_two_btn_text = $settings['ewa_hero_two_button1_text'];
		
		
       ?>
		<!-- Hero Two Area Start Here -->		
		   <div class="hero-two">
				<div class="grid grid-bleed">
				    <div class="col-md-4">
					    <div class="hero-two__outter">
							<div class="hero-two__image" style="background-image: url('<?php echo esc_url($hero_two_image); ?>');"></div> 
						</div> <!-- end of .hero-two__outter -->
					</div> <!-- end of .col -->
					<div class="col-md-8 align-self-center">
						<div class="hero-two__content">
							<h1><?php echo $hero_two_title; ?></h1>
							<a href="<?php echo esc_url($hero_two_btn_link);?>" title="<?php echo $hero_two_btn_text; ?>" class="btn-primary btn-primary-icon"><?php echo $hero_two_btn_text; ?></a>
						</div> <!-- end of .hero-two__content -->
					</div> <!-- end of .col -->
				</div> <!-- end of .grid -->
			</div> 
		<!-- Hero Two Area End Here -->
       <?php
	}
}