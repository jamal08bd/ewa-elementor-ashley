<?php
/**
 * EWA Elementor Cta Widget.
 *
 * Elementor widget that inserts cta into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_Cta_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve cta widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-cta-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve cta widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley Cta', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve cta widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the cta widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register cta widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// Cta Pre Heading
		$this->add_control(
		    'ewa_cta_pre_heading',
			[
			    'label' => esc_html__('Pre Heading','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Pre Heading','ewa-elementor-ashley'),
			]
		);
		
		// Cta Heading
		$this->add_control(
		    'ewa_cta_heading',
			[
			    'label' => esc_html__('Heading','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Heading','ewa-elementor-ashley'),
			]
		);
		
		// Cta Phone Number
		$this->add_control(
		    'ewa_cta_phone_number',
			[
			    'label' => esc_html__('Phone Number','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Phone Number','ewa-elementor-ashley'),
			]
		);
		
		// Cta Post Phone Text
		$this->add_control(
		    'ewa_cta_post_phone_text',
			[
			    'label' => esc_html__('Post Phone Text','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Post Phone Text','ewa-elementor-ashley'),
			]
		);
		
		// Cta Button1 Text
        $this->add_control(
        	'ewa_cta_button1_text',
			[
				'label'         => esc_html__('Cta Button Text', 'ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::TEXT,
				'label_block'   => true,
				'default'       => esc_html__('Enter Button1 Text','ewa-elementor-ashley'),
			]
        );
		
		//Cta Button1 Link
		$this->add_control(
		    'ewa_cta_button1_link',
			[
			    'label'         => esc_html__('Cta Button1 Link','ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// Cta Pre Heading Options
		$this->add_control(
			'ewa_cta_pre_heading_options',
			[
				'label' => esc_html__( 'Cta Pre Heading', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Cta Pre Heading Color
		$this->add_control(
			'ewa_cta_pre_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .cta__heading h5' => 'color: {{VALUE}}',
				],
			]
		);

		// Cta Pre Heading Bar Color
		$this->add_control(
			'ewa_cta_pre_heading_bar_color',
			[
				'label' => esc_html__( 'Bar Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .cta__heading h5::after' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Cta Pre Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_pre_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__heading h5',
			]
		);
		
		// Cta Heading Options
		$this->add_control(
			'ewa_cta_heading_options',
			[
				'label' => esc_html__( 'Cta Heading', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Cta Heading Color
		$this->add_control(
			'ewa_cta_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .cta__heading h2' => 'color: {{VALUE}}',
				],
			]
		);

		// Cta Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__heading h2',
			]
		);
		
		// Cta Phone Number Options
		$this->add_control(
			'ewa_cta_phone_number_options',
			[
				'label' => esc_html__( 'Cta Phone', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Cta Phone Number Color
		$this->add_control(
			'ewa_cta_phone_number_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .cta__contact span' => 'color: {{VALUE}}',
				],
			]
		);

		// Cta Phone Number Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_phone_number_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__contact span',
			]
		);
		
		// Cta Phone Post Text Options
		$this->add_control(
			'ewa_cta_phone_post_text_options',
			[
				'label' => esc_html__( 'Cta Phone Post Text', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Cta Phone Post Text Color
		$this->add_control(
			'ewa_cta_phone_post_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .cta__contact p' => 'color: {{VALUE}}',
				],
			]
		);

		// Cta Phone Post Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_phone_post_text_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta__contact p',
			]
		);
		
		// Cta Button Options
		$this->add_control(
			'ewa_cta_btn_options',
			[
				'label' => esc_html__( 'Cta Button', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// Cta Button Color
		$this->add_control(
			'ewa_cta_btn_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .cta a' => 'color: {{VALUE}}',
				],
			]
		);
		
		// Cta Button Background Color
		$this->add_control(
			'ewa_cta_btn_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .cta a' => 'background-color: {{VALUE}}',
				],
			]
		);

		// Cta Button Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_cta_btn_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta a',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);

		// Cta Button Hover Background Color
		$this->add_control(
			'ewa_cta_btn__hover_background_color',
			[
				'label' => esc_html__( 'Background-Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .cta__btn a:hover' => 'background-color: {{VALUE}} !important',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render cta widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		
		$cta_pre_heading = $settings['ewa_cta_pre_heading'];
		$cta_heading = $settings['ewa_cta_heading'];
		$cta_phone_number = $settings['ewa_cta_phone_number'];
		$cta_phone_text = $settings['ewa_cta_post_phone_text'];
		$cta_btn_link = $settings['ewa_cta_button1_link']['url'];
		$cta_btn_text = $settings['ewa_cta_button1_text'];
		
		
       ?>
		<!-- Cta Area Start Here -->		
		    <div class="cta">
                <div class="grid align-center">
				    <div class="col-md-5">
					    <div class="cta__heading">
						    <h5><?php echo $cta_pre_heading; ?></h5>
							<h2><?php echo $cta_heading; ?></h2>
						</div> <!-- end of .cta__heading -->
					</div> <!-- end of .col -->
				    <div class="col-md-4">
					    <div class="cta__contact">
						    <span><?php echo $cta_phone_number; ?></span>
						    <p><?php echo $cta_phone_text; ?></p>
						</div>
					</div> <!-- end of .col -->
				    <div class="col-md-3">
					    <div class="cta__btn">
							<a href="<?php echo esc_url($cta_btn_link);?>" title="<?php echo $cta_btn_text;?>" class="btn-primary btn-primary-icon"><?php echo $cta_btn_text;?></a>
						</div>
					</div> <!-- end of .col -->
				</div> <!-- end of .grid -->
			</div> <!-- end of .cta -->
		<!-- Cta Area End Here -->
       <?php
	}
}