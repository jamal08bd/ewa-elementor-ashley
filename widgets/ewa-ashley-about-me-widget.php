<?php
/**
 * EWA Elementor About Widget.
 *
 * Elementor widget that inserts about into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_About_Me_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve about widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-about-me-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve about widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley About Me', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve about widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the about widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register about widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// About Me Image
		$this->add_control(
		    'ewa_about_me_image',
			[
			    'label' => esc_html__('Choose About Me Image','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// About Me Description
		$this->add_control(
		    'ewa_about_me_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter About Me Description','ewa-elementor-ashley'),
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// About Description Options
		$this->add_control(
			'ewa_about_des_options',
			[
				'label' => esc_html__( 'About Description', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// About Description Color
		$this->add_control(
			'ewa_about_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .about p' => 'color: {{VALUE}}',
				],
			]
		);

		// About Me Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_about_desc_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-extension' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .about__content p',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render about widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$about_me_image = $settings['ewa_about_me_image']['url'];
		$about_me_des = $settings['ewa_about_me_des'];
		
		
       ?>
		<!-- About Area Start Here -->		
		<div class="about">
		    <div class="container-full">
				<div class="grid grid-bleed">
					<div class="col-lg-3 col-sm-4">
						<div class="about__images" style="background-image: url('<?php echo $about_me_image; ?>');"></div>
					</div> <!-- end of .col-lg -->
					<div class="col-lg-9 col-sm-8 align-self-center">
						<div class="about__content">
							<p><?php echo $about_me_des; ?></p>
						</div> <!-- end of .about__content -->
					</div> 
				</div> <!-- end of .grid -->
			</div> <!-- end of .container-full -->
		</div> <!-- end of .about -->
		<!-- About Area End Here -->
       <?php
	}
}