<?php
/**
 * EWA Elementor My Offer Widget.
 *
 * Elementor widget that inserts my offer into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_My_Offer_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve my offer  widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-my-offer-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve my offer  widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley My Offer', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve my offer  widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the my offer  widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register my offer  widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// My offer Background Image
		$this->add_control(
		    'ewa_my_offer_bg_image',
			[
			    'label' => esc_html__('Choose My offer Background Image','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		
		// My offer image
		$this->add_control(
		    'ewa_my_offer_image',
			[
			    'label' => esc_html__('Choose My offer Image','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
				    'url' => \Elementor\Utils::get_placeholder_image_src(),          
				],
			]
		);
		
		// My offer Description
		$this->add_control(
		    'ewa_my_offer_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter My offer Description','ewa-elementor-ashley'),
			]
		);
		
		// My offer repeater
		$repeater = new \Elementor\Repeater();

		// Repeater for Accordion Title
		$repeater->add_control(
			'ewa_my_offer_accordion_title',
			[
				'label' => esc_html__( 'Accordion Title', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Add New Accordion' , 'ewa-elementor-ashley' ),
			]
		);
		
		// Repeater for Accordion Inner
		$repeater->add_control(
			'ewa_my_offer_accordion_inner',
			[
				'label' => esc_html__( 'Accordion Inner', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'label_block' => true,
				'default' => esc_html__( 'Add New Accordion Inner' , 'ewa-elementor-ashley' ),
			]
		);
		
		//Accordion Icon Code
		$repeater->add_control(
		    'ewa_my_offer_accordion_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-ashley'),
			]
		);
		
		// Accordion List
		$this->add_control(
			'ewa_my_offer_accordion_list',
			[
				'label' => esc_html__( 'Accordion List', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ ewa_my_offer_accordion_title }}}',
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// My offer Description Options
		$this->add_control(
			'ewa_my_offer_des_options',
			[
				'label' => esc_html__( 'My offer Description', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My offer Description Color
		$this->add_control(
			'ewa_my_offer_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .my-offer__content h4' => 'color: {{VALUE}}',
				],
			]
		);

		// My offer Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_offer_desc_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .my-offer__content h4',
			]
		);
		
		// My offer Accordion Options
		$this->add_control(
			'ewa_my_offer_accordion_options',
			[
				'label' => esc_html__( 'My offer Accordion', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My offer Accordion Title Color
		$this->add_control(
			'ewa_my_offer_accordion_title_color',
			[
				'label' => esc_html__( 'Title Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} ul.ashley-accordion li a.accordion-toggle, .my-offer__icon' => 'color: {{VALUE}}',
				],
			]
		);

		// My offer Accordion Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_offer_accordion_title_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ul.ashley-accordion li a.accordion-toggle, .my-offer__icon',
			]
		);

		// My offer Accordion Active Title Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_offer_accordion_active_title_typography',
				'label' => esc_html__( 'Active Title Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ul.ashley-accordion li a.accordion-toggle.current',
			]
		);

		// My offer Accordion Description Color
		$this->add_control(
			'ewa_my_offer_accordion_desc_color',
			[
				'label' => esc_html__( 'Description Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} ul.ashley-accordion .accordion-inner, .accordion-inner p' => 'color: {{VALUE}} !important',
				],
			]
		);

		// My offer Accordion Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_offer_accordion_desc_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}}  ul.ashley-accordion .accordion-inner, ul.ashley-accordion .accordion-inner p'
			]
		);

		// My offer Accordion Background Color
		$this->add_control(
			'ewa_my_offer_accordion_back_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} ul.ashley-accordion li, ul.ashley-accordion li a.accordion-toggle.current' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render my offer  widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		$my_offer_bg_image = $settings['ewa_my_offer_bg_image']['url'];
		$my_offer_image = $settings['ewa_my_offer_image']['url'];
		$my_offer_description = $settings['ewa_my_offer_des'];
		
		?>
		
		<!-- My Offer Area Start Here -->		
		    <div class="my-offer">
				<div class="container-full">
					<div class="grid grid-bleed">
						<div class="col-lg-3 col-sm-4">
							<div class="my-offer__image" style="background-image: url('<?php echo $my_offer_image; ?>');">
							    <div class="my-offer__bg" style="background-image: url('<?php echo $my_offer_bg_image; ?>');"></div>
							</div>   
						</div> <!-- end of .col-lg-3 -->
						<div class="col-lg-9 col-sm-8">
							<div class="my-offer__content">
								<h4><?php echo $my_offer_description; ?></h4>					    
								<!-- offer list -->
								<ul class="ashley-accordion">
									<?php 

									$count = 0;
									// we will define which one needs to be opened initially, in this case we will define 1st accordion as current faq
									$open_faq_class = '';
									$open_faq_inner = '';

									foreach (  $settings['ewa_my_offer_accordion_list'] as $item ) { 
										$my_offer_accordion_title = $item['ewa_my_offer_accordion_title'];
										$my_offer_accordion_inner = $item['ewa_my_offer_accordion_inner'];
										$my_offer_accordion_icon = $item['ewa_my_offer_accordion_icon_code'];

										// define 1st element as initial open element (DO NOT REMOVE THIS, THIS NEED SOME FIX)
										if ($count == 0) {
											$open_faq_class = ' current';
											$open_faq_inner = ' show';
										} else{
											$open_faq_class = '';
											$open_faq_inner = '';
										}
									?>								
										<li>									
											<a class="accordion-toggle<?php echo esc_attr($open_faq_class); ?>" href="javascript:void(0);">
												<div class="my-offer__icon">
													<?php echo $my_offer_accordion_icon; ?>
												</div> <!-- .my-offer__icon -->
												<?php echo $my_offer_accordion_title;?>
											</a>
											<div class="accordion-inner<?php echo esc_attr($open_faq_inner); ?>">
												<?php echo $my_offer_accordion_inner;?>
											</div>
										</li>
									<?php 
									$count++;
									} 
									?>
								</ul><!-- .end of .ashley-accordion -->	
							</div> <!-- end of .my-offer__content -->							
						</div> <!-- end of .col-lg-9 -->
					</div> <!-- .grid end here -->
				</div> <!-- end of .container-full -->
			</div> 
		<!-- My Offer Area End Here -->
       <?php
	}
}