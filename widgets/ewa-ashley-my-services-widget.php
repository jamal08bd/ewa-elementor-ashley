<?php
/**
 * EWA Elementor My Services Widget.
 *
 * Elementor widget that inserts my services into the page
 *
 * @since 1.0.0
 */
class EWA_Ashley_My_Services_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve my services widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ewa-ashley-my-services-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve my services widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'EWA Ashley My Services', 'ewa-elementor-ashley' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve my services widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'far fa-address-card';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the my services widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ewa-ashley' ];
	}

	/**
	 * Register my services widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		
		// start of the Content tab section
	   $this->start_controls_section(
	       'content-section',
		    [
		        'label' => esc_html__('Content','ewa-elementor-ashley'),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
		   
		    ]
	    );
		
		// My Services Pre Heading
		$this->add_control(
		    'ewa_my_services_pre_heading',
			[
			    'label' => esc_html__('Pre Heading','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Pre Heading','ewa-elementor-ashley'),
			]
		);
		
		// My Services Heading
		$this->add_control(
		    'ewa_my_services_heading',
			[
			    'label' => esc_html__('Heading','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Heading','ewa-elementor-ashley'),
			]
		);
		
		// My Services Description
		$this->add_control(
		    'ewa_my_services_des',
			[
			    'label' => esc_html__('Description','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Description','ewa-elementor-ashley'),
			]
		);
		
		// My Services Icon Code
		$this->add_control(
		    'ewa_my_services_icon_code',
			[
			    'label' => esc_html__('Icon','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Icon Code','ewa-elementor-ashley'),
			]
		);
		
		//My Services Icon Link
		$this->add_control(
		    'ewa_my_services_icon_link',
			[
			    'label'         => esc_html__('My Services Icon Link','ewa-elementor-ashley'),
				'type'          => \Elementor\Controls_Manager::URL,
				'label_block'   => true,
				'default'       => [
				    'url'   => '#',
				],
			]
		);
		
		// My Services Icon Post Text
		$this->add_control(
		    'ewa_my_services_icon_post_text',
			[
			    'label' => esc_html__('Post Icon Text','ewa-elementor-ashley'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__('Enter Post Icon Text','ewa-elementor-ashley'),
			]
		);
		
		$this->end_controls_section();
		// end of the Content tab section
		
		// start of the Style tab section
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Content Style', 'ewa-elementor-ashley' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'style_tabs'
		);
		
		// start everything related to Normal state here
		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'ewa-elementor-ashley' ),
			]
		);
		
		// My Services Pre Heading Options
		$this->add_control(
			'ewa_my_services_pre_heading_options',
			[
				'label' => esc_html__( 'My Services Pre Heading', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My Services Pre Heading Bar Color
		$this->add_control(
			'ewa_my_services_pre_heading_bar_color',
			[
				'label' => esc_html__( 'Bar Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .my-services__preheading::after' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		// My Services Pre Heading Color
		$this->add_control(
			'ewa_my_services_pre_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .my-services__items h5' => 'color: {{VALUE}}',
				],
			]
		);

		// My Services Pre Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_services_pre_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .my-services__preheading span',
			]
		);
		
		// My Services Heading Options
		$this->add_control(
			'ewa_my_services_heading_options',
			[
				'label' => esc_html__( 'My Services Heading', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My Services Heading Color
		$this->add_control(
			'ewa_my_services_heading_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .my-services__items h2' => 'color: {{VALUE}}',
				],
			]
		);

		// My Services Heading Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_services_heading_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .my-services__items h2',
			]
		);
		
		// My Services Description Options
		$this->add_control(
			'ewa_my_services_des_options',
			[
				'label' => esc_html__( 'My Services Phone Post Text', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My Services Description Color
		$this->add_control(
			'ewa_my_services_des_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#1D282E',
				'selectors' => [
					'{{WRAPPER}} .my-services__text' => 'color: {{VALUE}}',
				],
			]
		);

		// My Services Description Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_services_des_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .my-services__text',
			]
		);
		
		// My Services Icon Options
		$this->add_control(
			'ewa_my_services_btn_options',
			[
				'label' => esc_html__( 'My Services Icon', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My Services Icon Color
		$this->add_control(
			'ewa_my_services_icon_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .my-services__items i' => 'color: {{VALUE}}',
				],
			]
		);
		
		// My Services Icon Background Color
		$this->add_control(
			'ewa_my_services_icon_post_text_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FCD7D9',
				'selectors' => [
					'{{WRAPPER}} .my-services__items a' => 'background-color: {{VALUE}}',
				],
			]
		);

		// My Services Icon Post Text Options
		$this->add_control(
			'ewa_my_services_icon_text_options',
			[
				'label' => esc_html__( 'My Services Icon Post Text', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		// My Services Icon Post Text Color
		$this->add_control(
			'ewa_my_services_icon_text_color',
			[
				'label' => esc_html__( 'Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#450A0D',
				'selectors' => [
					'{{WRAPPER}} .my-services__items p' => 'color: {{VALUE}}',
				],
			]
		);

		// My Services Icon Post Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'ewa_my_services_icon_post_text_typography',
				'label' => esc_html__( 'Typography', 'ewa-elementor-ashley' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .my-services__items p',
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Normal state here

		// start everything related to Hover state here
		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'ewa-elementor-ashley' ),
			]
		);

		// My Services Icon Hover Background
		$this->add_control(
			'ewa_my_services_icon_post_text_hover_color',
			[
				'label' => esc_html__( 'Background Color', 'ewa-elementor-ashley' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'default' => '#FFF3F3',
				'selectors' => [
					'{{WRAPPER}} .my-services__items a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		// end everything related to Hover state here

		$this->end_controls_tabs();

		$this->end_controls_section();
		// end of the Style tab section

	}

	/**
	 * Render my services widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();
		
		
		$my_services_pre_heading = $settings['ewa_my_services_pre_heading'];
		$my_services_heading = $settings['ewa_my_services_heading'];
		$my_services_description = $settings['ewa_my_services_des'];
		$my_services_icon = $settings['ewa_my_services_icon_code'];
		$my_services_icon_link = $settings['ewa_my_services_icon_link']['url'];
		$my_services_icon_post_text = $settings['ewa_my_services_icon_post_text'];
		
		
		
       ?>
		<!-- My Services Area Start Here -->		
		    <div class="my-services">
                <div class="grid">
				    <div class="col-sm-12">
					    <div class="my-services__items">
							<h5 class="my-services__preheading"><span><?php echo $my_services_pre_heading; ?></span></h5>
							<h2><?php echo $my_services_heading; ?></h2>
							<div class="my-services__text"><?php echo $my_services_description; ?></div>
							<a href="<?php echo esc_url($my_services_icon_link);?>" title="<?php echo $my_services_icon_post_text; ?>" class="btn-primary"><?php echo $my_services_icon; ?></a>
							<p><?php echo $my_services_icon_post_text; ?></p>
						</div>
					</div> <!-- end of .col -->
				</div> <!-- end of .grid -->
			</div> <!-- end of .my-services -->
		<!-- My Services Area End Here -->
       <?php
	}
}